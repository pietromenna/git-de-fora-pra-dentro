# Git de fora pra dentro

Repositorio para a apresentação na [FISL 2018](https://agenda.fisl18.softwarelivre.org/#/).

Os slides renderizados estão disponivels [aqui](http://naoesquecepietrodecolocaroenderecoaqui.com).

## Agradecimentos

A [Mary Rose Cook](https://maryrosecook.com) que criou a apresentação original em ingles e apresentou para a turma de Spring 2 2015 no [Recurse Center](https://recurse.com). Link [aqui](https://codewords.recurse.com/issues/two/git-from-the-inside-out).
Aos meus colegas e revisores: [Douglas Vaghetti](https://github.com/vaghetti) e [Lucas Xavier](https://github.com/lucasmsx) que revisaram e corrigiram meus erros de portugues.